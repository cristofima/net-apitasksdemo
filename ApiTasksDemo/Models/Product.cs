﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ApiTasksDemo.Models
{
    public class Product
    {
        [Key]
        public int ProductId { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public double Price { get; set; }

        public bool IsFragile { get; set; }

        [Required]
        public DateTime ExpirationDate { get; set; }

        public string Description { get; set; }

        [Required]
        public int Stock { get; set; }

        [Required]
        public int CategoryId { get; set; }
        public string Image { get; set; }
    }
}
