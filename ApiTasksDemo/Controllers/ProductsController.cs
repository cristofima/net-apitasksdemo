﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ApiTasksDemo.Models;
using ApiTasksDemo.DTO.Requests;
using System.Collections;

namespace ApiTasksDemo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly TestContext _context;

        public ProductsController(TestContext context)
        {
            _context = context;
        }

        // GET: api/Products
        [HttpGet]
        public async Task<ActionResult<IList>> GetProducts()
        {
            return await _context.Products
                .Select(x => new
                {
                    x.ProductId,
                    x.IsFragile,
                    x.Name,
                    x.Price,
                    x.Stock,
                    x.CategoryId
                })
                .ToListAsync();
        }

        // GET: api/Products/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Product>> GetProduct(int id)
        {
            var product = await _context.Products.FindAsync(id);

            if (product == null)
            {
                return NotFound();
            }

            return product;
        }

        // PUT: api/Products/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProduct(int id, Product product)
        {
            if (id != product.ProductId)
            {
                return BadRequest();
            }

            _context.Entry(product).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Products
        [HttpPost]
        public async Task<ActionResult<Product>> PostProduct(ProductRequest request)
        {
            Product product;

            if (request.ProductId <= 0)
            {
                product = new Product
                {
                    CategoryId = request.CategoryId,
                    Description = request.Description,
                    ExpirationDate = request.ExpirationDate,
                    IsFragile = request.IsFragile,
                    Name = request.Name,
                    Price = request.Price,
                    Stock = request.Stock,
                    Image = request.Image
                };

                _context.Products.Add(product);
            }
            else
            {
                product = _context.Products.Find(request.ProductId);
                if (product != null)
                {
                    product.CategoryId = request.CategoryId;
                    product.Description = request.Description;
                    product.ExpirationDate = request.ExpirationDate;
                    product.IsFragile = request.IsFragile;
                    product.Name = request.Name;
                    product.Price = request.Price;
                    product.Stock = request.Stock;
                    product.Image = request.Image;

                    _context.Update(product);
                }
            }

            await _context.SaveChangesAsync();

            return Ok(product);
        }

        // DELETE: api/Products/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteProduct(int id)
        {
            var product = await _context.Products.FindAsync(id);
            if (product == null)
            {
                return NotFound();
            }

            _context.Products.Remove(product);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool ProductExists(int id)
        {
            return _context.Products.Any(e => e.ProductId == id);
        }
    }
}
